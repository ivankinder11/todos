import firebase from "firebase/compat/app";
import 'firebase/compat/database';
import {useEffect, useState} from "react";

function Main() {
    const [todoList, setTodoList] = useState([])

    useEffect(() => {
        const db = firebase.database();
        const data = db.ref('data');
        data.on('value', elem => setTodoList(elem.val()));
    }, []);

    return (
        <div className="App">
            <div>
                <h1>Список заметок</h1>
                <div>
                    {todoList.map((el, index) =>
                        <div key={index}>{el.name}</div>)
                    }
                </div>
            </div>
        </div>
    );
}

export default Main;
